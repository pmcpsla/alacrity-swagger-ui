# Introduction

This component exposes REST API for Alacrity Direct Operator Billing on Swagger - see https://alacrity-swagger-ui.aerobatic.io/#/

For documentation on Swagger see http://swagger.io/

For documentation on how to host and update our Swagger APIs see https://www.aerobatic.com/blog/hosting-swagger-api-documentation-wth-bitbucket 



## Alacrity Swagger

This component consists of a clone of Swagger-UI. Swagger-UI takes a JSON file, representing an API, and generates html pages. The JSON file is called alacrity.json. 
This file was generated from the Swagger-editor which was used to create a YAML file, called alacrity.yaml, from which JSON is generated. 
This compnent has been generated following the instructions in https://www.aerobatic.com/blog/hosting-swagger-api-documentation-wth-bitbucket


## Random Notes

### CORS errors need resolved
See https://www.thepolyglotdeveloper.com/2014/08/bypass-cors-errors-testing-apis-locally/ 

### Authentication
To authenticate your service calls use this as the key “Basic c2xhX3Rlc3R6YV8yMDlfc2JveDpuZ2l0b2xpM3U=”; select the Authorize Button.
Not sure why I need to add the word Basic but it works


